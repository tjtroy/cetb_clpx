from netCDF4 import Dataset, num2date
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# function brings in the MET data from the CLPX ISA Corner MET stations and returns the data as a pd.Dataframe, function indexes the frame with datetimes (data in one-hour intervals)
# station data of interest includes time series of snow depths and air temps.  User inputs site and corner as strings, use two-letter CLPX code for ISA and two-letter code for corner
# For example, passing: site_ID='FSSW'  would refer to the Fraser St Louis Creek (FS) site and the southwest corner station (SW)
def read_corner_station(site_ID):
	
	# read in the station data csv	
	Station=pd.read_csv('/home/shared/Data3/cetb/CLPX/CLPX_Data/CornerMET/'+site_ID+'_L1master_v1.0.csv', header=None, sep=',')

	#replace fill value with NaN, 8999 is a missing reading
	Station[Station==8999.0] = np.NaN
	Station[Station==8999] = np.NaN
	Station[Station=='8999'] = np.NaN
	
	# add column names from headers file
	Station.columns=[pd.read_csv('/home/shared/Data3/cetb/CLPX/CLPX_Data/CornerMET/L1headers_v1.0.csv')]
	
	# some major hacking here to index with dates in one-hour intervals, but it works	
	a=tuple(Station['Time'].iloc[0])   #gets the first cell in the 'Time' column and saves as a tuple for use in next line
	start=pd.to_datetime(str(int(Station['Year'].iloc[0]))+'-'+str(int(Station['Month'].iloc[0]))+'-'+str(int(Station['Day'].iloc[0]))+' '+a[0])    # get the first cell in the Year, Month, and Day columns and convert it to a datetime to use at the start of the daterange below
	b=tuple(Station['Time'].iloc[-1])  #gets the last cell in the 'Time' column and saves as a tuple for use in next line
	end=pd.to_datetime(str(int(Station['Year'].iloc[-1]))+'-'+str(int(Station['Month'].iloc[-1]))+'-'+str(int(Station['Day'].iloc[-1]))+' '+b[0])  # get the last cell in the Year, Month, and Day columns and convert it to a datetime to use at the end of the daterange below
	Station=Station.set_index(pd.date_range(start, end, freq='H'))   # create a daterange to use as the index for dataframe, uses previously specified start and end with hourly frequency
	Station['Snow Depth (m)']=Station['Snow Depth (m)'].convert_objects(convert_numeric=True)   #index the dataframe
	return Station

# for reading in the MAIN MET station data and indexing with appropriate dates
def compose_date(years, months=1, days=1, weeks=None, hours=1, minutes=1,
                 seconds=None, milliseconds=None, microseconds=None, nanoseconds=None):
    years = np.asarray(years) - 1970
    months = np.asarray(months) - 1
    days = np.asarray(days) - 1
    hours= np.asarray(hours)
    types = ('<M8[Y]', '<m8[M]', '<m8[D]', '<m8[W]', '<m8[h]',
             '<m8[m]', '<m8[s]', '<m8[ms]', '<m8[us]', '<m8[ns]')
    vals = (years, months, days, weeks, hours, minutes, seconds,
            milliseconds, microseconds, nanoseconds)
    return sum(np.asarray(v, dtype=t) for t, v in zip(types, vals)
               if v is not None)
